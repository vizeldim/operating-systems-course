#ifndef __PROGTEST__
#include <cstdio>
#include <cstdlib>
#include <cstdint>
#include <cstring>
#include <pthread.h>
#include <semaphore.h>
#include "common.h"
#include "iostream"
using namespace std;
#endif /* __PROGTEST__ */


pthread_mutex_t mx;
pthread_attr_t attr;
pthread_t threads[PROCESS_MAX];
int freeThreadsStack[PROCESS_MAX];
uint32_t threadsTop = PROCESS_MAX - 1;

class CSimpleCPU : public CCPU
{
  public:
                             CSimpleCPU                    ( uint8_t   * memStart,
                                                             uint32_t pageTableRoot,
                                                             uint32_t * freePageStack,
                                                             uint32_t * top,
                                                             uint32_t totalPages);
    virtual uint32_t         GetMemLimit                   ( void ) const;
    virtual bool             SetMemLimit                   ( uint32_t          pages );
    virtual bool             NewProcess                    ( void            * processArg,
                                                             void           (* entryPoint) ( CCPU *, void * ),
                                                             bool              copyMem );
  private:
    uint32_t m_TotalPages;
    uint32_t * m_FreePagesStack;
    uint32_t *m_Top;

    bool addPages(uint32_t pagesToAdd);
    bool removePages(uint32_t pagesToRemove);

    uint32_t m_RootTableSize;
    uint32_t m_LastPageTableSize;

    uint32_t getFreePageLink();
    void addToLastPageTable();
    void createNewPageTable();

    uint32_t * getPageTableLastAddr();
    uint32_t * getLastPageAddr();

    void removeLastPageTable();

    void removeFromLastPageTable(uint32_t pagesToRemove);

    void copyMemory(CSimpleCPU * to, uint32_t rootTableLink);
};

uint32_t CSimpleCPU::GetMemLimit() const
{
    if(m_RootTableSize > 0)
        return (m_RootTableSize - 1) * PAGE_DIR_ENTRIES + m_LastPageTableSize;
    else
        return m_LastPageTableSize;
}

bool CSimpleCPU::SetMemLimit(uint32_t pages)
{
    uint32_t currentPages = GetMemLimit();

    if (currentPages > pages)
    {
        if(!removePages(currentPages - pages))
            return false;
    }
    else if(currentPages < pages)
    {
        if(!addPages(pages - currentPages))
            return false;
    }

    return true;
}

uint32_t CSimpleCPU::getFreePageLink() // Link == offset to uint8_t pointer !!! --- 8 BIT POINTER --- !!!
{
    uint32_t curTop = *m_Top;
    --(*m_Top);
    return (m_FreePagesStack[curTop] * PAGE_SIZE) | BIT_WRITE | BIT_PRESENT | BIT_USER;
}

uint32_t * CSimpleCPU::getPageTableLastAddr()
{
    if (m_RootTableSize == 0)
        return (uint32_t *)(m_MemStart + (m_PageTableRoot & ADDR_MASK));
    else
        return (uint32_t *)(m_MemStart + (m_PageTableRoot & ADDR_MASK)) + (m_RootTableSize - 1);
}

uint32_t * CSimpleCPU::getLastPageAddr()
{ return (uint32_t *)(m_MemStart + (*(getPageTableLastAddr()) & ADDR_MASK )) + m_LastPageTableSize; }


void CSimpleCPU::addToLastPageTable()
{
    uint32_t * lastPageAddr = getLastPageAddr();

    pthread_mutex_lock(&mx);
    *lastPageAddr = getFreePageLink();
    pthread_mutex_unlock(&mx);

    ++m_LastPageTableSize;
}

void CSimpleCPU::createNewPageTable()
{
    uint32_t * pageTableNewAddr = (uint32_t *)(m_MemStart + (m_PageTableRoot & ADDR_MASK)) + m_RootTableSize;

    pthread_mutex_lock(&mx);
    *pageTableNewAddr = getFreePageLink();
    pthread_mutex_unlock(&mx);

    ++m_RootTableSize;
    m_LastPageTableSize = 0;
}

bool CSimpleCPU::addPages(uint32_t pagesToAdd)
{

    if (pagesToAdd > (*m_Top)) //Not enough pages
    {
        return false;
    }
   while (pagesToAdd > 0)
   {
       if(m_RootTableSize > 0 && m_LastPageTableSize < PAGE_DIR_ENTRIES)
       {
           addToLastPageTable();
           --pagesToAdd;
       }
       else
       { createNewPageTable(); }
   }

    return true;
}

void CSimpleCPU::removeLastPageTable()
{
    pthread_mutex_lock(&mx);

    uint32_t * lastPageTableAddr = getPageTableLastAddr();
    uint32_t * lastPageTablePageLink = (uint32_t *)(m_MemStart + (*(lastPageTableAddr) & ADDR_MASK ));


    for (uint32_t i = 0; i < m_LastPageTableSize; ++i) {
        ++(*m_Top);
        m_FreePagesStack[(*m_Top)] = (*(lastPageTablePageLink + i)) >> OFFSET_BITS;
    }
    memset(lastPageTablePageLink, 0, m_LastPageTableSize*4);

    ++(*m_Top);
    m_FreePagesStack[(*m_Top)] = (*lastPageTableAddr) >> OFFSET_BITS;

    *lastPageTableAddr = 0;

    pthread_mutex_unlock(&mx);

    --m_RootTableSize;

    if (m_RootTableSize > 0)
        m_LastPageTableSize = PAGE_DIR_ENTRIES;
    else
        m_LastPageTableSize = 0;

}

void CSimpleCPU::removeFromLastPageTable(uint32_t pagesToRemove)
{
    pthread_mutex_lock(&mx);

    uint32_t * lastPageTableAddr = (uint32_t *)(m_MemStart + (*(getPageTableLastAddr()) & ADDR_MASK )) + (m_LastPageTableSize - pagesToRemove);
    for (uint32_t i = 0; i < pagesToRemove; ++i) {
        ++(*m_Top);
        m_FreePagesStack[(*m_Top)] = (*(lastPageTableAddr + i)) >> OFFSET_BITS;
    }
    memset(lastPageTableAddr, 0, pagesToRemove * 4);
    m_LastPageTableSize -= pagesToRemove;

    pthread_mutex_unlock(&mx);
}

bool CSimpleCPU::removePages(uint32_t pagesToRemove)
{
    while (pagesToRemove > 0)
    {
        if(pagesToRemove >= m_LastPageTableSize)
        {
            pagesToRemove -= m_LastPageTableSize;
            removeLastPageTable();
        }
        else
        {
            removeFromLastPageTable(pagesToRemove);
            pagesToRemove = 0;
        }
    }

    return true;
}

struct SArgs{
    SArgs(int threadIndex, CCPU * cpu, void (*entryPoint)(CCPU *, void *),void *processArg)
    {
        this -> threadIndex = threadIndex;
        this -> cpu = cpu;
        this -> processArg = processArg;
        this -> entryPoint = entryPoint;
    }

    int threadIndex;
    CCPU * cpu;
    void * processArg;
    void (* entryPoint)(CCPU *, void *);
};


void *singleThread(void * args)
{

    SArgs * sargs = (SArgs *)args;

    sargs -> entryPoint(sargs -> cpu, sargs -> processArg);


    delete sargs -> cpu;
    freeThreadsStack[++threadsTop] = sargs -> threadIndex;
    delete sargs;


    return nullptr;
}

void CSimpleCPU::copyMemory(CSimpleCPU * to, uint32_t rootTableLink)
{
    uint32_t pages = GetMemLimit();

    to -> SetMemLimit(pages);
    for (uint32_t i = 0; i < m_RootTableSize - 1; ++i)
    {
        uint32_t * rootTable = (uint32_t *)(m_MemStart + (m_PageTableRoot & ADDR_MASK)) + i;
        uint32_t * rootTableTo = (uint32_t *)(m_MemStart + (rootTableLink & ADDR_MASK)) + i;

        for (uint32_t j = 0; j < PAGE_DIR_ENTRIES; ++j) {
            uint32_t * pageTable = (uint32_t *)(m_MemStart + (*rootTable & ADDR_MASK )) + j;
            uint32_t * valueAddr = (uint32_t *)(m_MemStart + (*pageTable & ADDR_MASK));

            uint32_t * pageTableTo = (uint32_t *)(m_MemStart + (*rootTableTo & ADDR_MASK )) + j;
            uint32_t * valueAddrTo = (uint32_t *)(m_MemStart + (*pageTableTo & ADDR_MASK));

          
            memcpy(valueAddrTo, valueAddr, PAGE_SIZE);

        }
    }
    for (uint32_t j = 0; j < m_LastPageTableSize; ++j) {

        uint32_t * rootTable = (uint32_t *)(m_MemStart + (m_PageTableRoot & ADDR_MASK)) + (m_RootTableSize - 1);
        uint32_t * pageTable = (uint32_t *)(m_MemStart + (*rootTable & ADDR_MASK )) + j;
        uint32_t * valueAddr = (uint32_t *)(m_MemStart + (*pageTable & ADDR_MASK));

        uint32_t * rootTableTo = (uint32_t *)(m_MemStart + (rootTableLink & ADDR_MASK)) + (m_RootTableSize - 1);
        uint32_t * pageTableTo = (uint32_t *)(m_MemStart + (*rootTableTo & ADDR_MASK )) + j;
        uint32_t * valueAddrTo = (uint32_t *)(m_MemStart + (*pageTableTo & ADDR_MASK));

        memcpy(valueAddrTo, valueAddr, PAGE_SIZE);
    }
}

bool CSimpleCPU::NewProcess(void *processArg, void (*entryPoint)(CCPU *, void *), bool copyMem)
{
    pthread_mutex_lock(&mx);

    if (threadsTop <= 0)
        return false;
    uint32_t freePageLink = getFreePageLink();

    CSimpleCPU * newCPU = new CSimpleCPU(m_MemStart, freePageLink, m_FreePagesStack, m_Top, m_TotalPages);

    pthread_mutex_unlock(&mx);

    if(copyMem && m_RootTableSize > 0) 
    { copyMemory(newCPU, freePageLink); }

    int threadIndex = freeThreadsStack[threadsTop--];
    SArgs * args = new SArgs(threadIndex ,newCPU, entryPoint, processArg);


    pthread_create(&threads[threadIndex], &attr, singleThread, args);

    return true;
}


void               MemMgr                                  ( void            * mem,
                                                             uint32_t          totalPages,
                                                             void            * processArg,
                                                             void           (* mainProcess) ( CCPU *, void * ))
{
    pthread_mutex_init(&mx, nullptr);
    pthread_attr_init(&attr);
    pthread_attr_setdetachstate(&attr,PTHREAD_CREATE_JOINABLE);

    --threadsTop;
    for (uint32_t i = 0; i < threadsTop; ++i)
    { freeThreadsStack[i] = i; }


    uint32_t * m_FreePagesStack = new uint32_t[totalPages];
    uint32_t * stackTop = new uint32_t();
    for (uint32_t i = 0; i < totalPages; ++i)
    { m_FreePagesStack[i] = i; }

    memset(mem, 0, totalPages * CCPU::PAGE_SIZE);

    uint32_t rta = (m_FreePagesStack[totalPages - 1] * CCPU::PAGE_SIZE);
    *stackTop = totalPages - 2;
    CSimpleCPU * simpleCpu = new CSimpleCPU((uint8_t *)mem, rta, m_FreePagesStack, stackTop, totalPages);

    mainProcess(simpleCpu, processArg);

    for (pthread_t &t : threads)
        pthread_join(t, nullptr);


    delete stackTop;
    delete simpleCpu;
    delete [] m_FreePagesStack;
}


CSimpleCPU::CSimpleCPU(uint8_t *memStart,
                       uint32_t pageTableRoot,
                       uint32_t * freePageStack,
                       uint32_t *top,
                       uint32_t totalPages) : CCPU(memStart, pageTableRoot)
{
    m_LastPageTableSize = 0;
    m_RootTableSize = 0;
    m_TotalPages = totalPages;
    m_FreePagesStack = freePageStack;
    m_Top = top;
}

